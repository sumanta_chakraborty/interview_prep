# require 'pry' # - required for debugging.

# Offers tools to calculate maximum profit
module ProfitMaximizer
  def self.get_max_profit(stock_prices_yesterday)
    # NOTE: Given the problem space, parameter validation is unnecessary.
    #       However, if used standalone, we could uncomment the following. Test
    #       are accordingly marked skipped.
    # NOTE: YAGNI flouted intentionally for interview prep purposes.
    #
    # raise ArgumentError, 'Stock prices are numbers!' if
    #   stock_prices_yesterday.any? { |quote| !quote.is_a? Numeric }
    # raise ArgumentError, 'Should be an enumerable with at least 2 items' if
    #   stock_prices_yesterday.count < 2
    # raise ArgumentError, 'Stock prices are never negative!' if
    #   stock_prices_yesterday.any? { |quote| quote < 0 }

    buy, sell = stock_prices_yesterday[0..1]
    could_buy = buy

    stock_prices_yesterday.drop(1).each do |quote|
      if quote - could_buy > sell - buy
        sell = quote
        buy = could_buy
      end
      could_buy = quote if could_buy > quote
    end
    sell - buy
  end
end
