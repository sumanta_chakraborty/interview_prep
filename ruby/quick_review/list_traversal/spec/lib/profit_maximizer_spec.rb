require 'profit_maximizer'

test_cases = [
  { rationale: 'provided example', input: [27, 3, 9, 6, 18, 1], result: 15 },
  { rationale: 'edge: last - first', input: [3, 9, 6, 18, 33], result: 30 },
  { rationale: 'edge: end', input: [10, 9, 8, 6, 10], result: 4 },
  { rationale: 'edge: start', input: [3, 10, 6, 8, 9], result: 7 },
  { rationale: 'constant', input: [9, 9, 9, 9, 9], result: 0 },
  { rationale: 'edge: loss @ end', input: [9, 7, 5, 2, 1], result: -1 },
  { rationale: 'edge: loss @ start', input: [9, 8, 6, 4, 2], result: -1 },
  { rationale: 'edge: 1 gain among loss', input: [9, 8, 6, 7, 2], result: 1 }
]

RSpec.describe ProfitMaximizer do
  describe '.get_max_profit' do
    test_cases.each do |test|
      it "#{test[:rationale]}: #{test[:input]} returns #{test[:result]}" do
        expect(ProfitMaximizer.get_max_profit(test[:input])).to eq test[:result]
      end
    end
    it 'raises error if less than 2 items', skip: 'usecase review' do
      expect { ProfitMaximizer.get_max_profit([0]) }.to raise_error(
        ArgumentError, /2/
      )
    end
    it 'raises error if -ve numbers are provided', skip: 'usecase review' do
      expect { ProfitMaximizer.get_max_profit([0, -2]) }.to raise_error(
        ArgumentError, /negative/
      )
    end
    it 'raises error if alphabets are provided', skip: 'usecase review' do
      expect { ProfitMaximizer.get_max_profit([0, 'a', 2]) }.to raise_error(
        ArgumentError, /numbers/
      )
    end
  end
end
