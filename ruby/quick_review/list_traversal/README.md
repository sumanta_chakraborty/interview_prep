# [Problem](https://www.interviewcake.com/question/stock-price)
Writing programming interview questions hasn't made me rich. Maybe trading Apple stocks will.
Suppose we could access yesterday's stock prices as a list, where:

The indices are the time in minutes past trade opening time, which was 9:30am local time.
The values are the price in dollars of Apple stock at that time.
So if the stock cost $500 at 10:30am, stock_prices_yesterday[60] = 500.

Write an efficient function that takes stock_prices_yesterday and returns the best profit I could have made from 1 purchase and 1 sale of 1 Apple stock yesterday.

For example:
```
  stock_prices_yesterday = [10, 7, 5, 8, 11, 9]

get_max_profit(stock_prices_yesterday)
# returns 6 (buying for $5 and selling for $11)
```
No "shorting"—you must buy before you sell. You may not buy and sell in the same time step (at least 1 minute must pass).

Do you have an answer?

Copyright © 2017 Cake Labs, Inc. All rights reserved.
228 Park Ave S #82632, New York, NY US 10003 (804) 876-2253

# Considerations:
## Follow up questions:
* What happens when the data is in descending order?
* What happens if the data remains constant, e.g. market was closed for Christmas?

## Assumptions:
* When the data is in descending order, return smallest loss.
* When the data is constant return 0 as you are not flouting the rule of not buying and selling in the same minute.

## Implementation:
The method requested was placed in [./lib/profit_maximizer.rb](./lib/profit_maximizer.rb).
Running the automated tests results in the following output:
![Alt text](./results.png)
