# Hi:
I am [Sumanta Chakraborty](https://www.linkedin.com/in/schak9).
# This Repo:
... will track some of my practice work before tech interviews.
I have never really had a public repo, which is important these days to show case expertise in software. So, better late than never. :)
# Table of Contents:
* In order to get started, I picked the first question in the first Google search result for "coding interview questions". I chose Ruby to solve it as that is the language I have been playing in lately at my job as DevOps for Oracle. - ([./ruby/quick_review/list_traversal](./ruby/quick_review/list_traversal))
* ... soon to come ...
